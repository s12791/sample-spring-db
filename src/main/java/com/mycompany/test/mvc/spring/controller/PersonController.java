
package com.mycompany.test.mvc.spring.controller;


import com.mycompany.test.mvc.spring.dao.PersonDAO;
import com.mycompany.test.mvc.spring.model.Person;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/person")
public class PersonController {
    
    @Autowired
    private PersonDAO personDAO;
    
    @RequestMapping("/add")
    public ModelAndView add(){
        ModelAndView model = new ModelAndView("add");
        model.addObject("person", new Person());
        return model;        
    }
    @RequestMapping(value="/save", method = RequestMethod.POST)
    public String save(@Valid Person person, BindingResult bindingResult){
        System.out.println(person);
        
        if(bindingResult.hasErrors()){
            return "add";
        }
        personDAO.save(person);
            return "redirect:/home.htm";
    }
}
