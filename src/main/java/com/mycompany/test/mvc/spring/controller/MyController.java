
package com.mycompany.test.mvc.spring.controller;

import com.mycompany.test.mvc.spring.model.Person;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MyController {
    
    protected Logger logger = Logger.getLogger(getClass());
    
    @RequestMapping("/home")
    public ModelAndView home() {
        
        logger.debug("MyController.home()");
    
        ModelAndView model = new ModelAndView("home");
        Person p = new Person();
        p.setName("Rafal");
        
        model.addObject("person",p);
        
        return model;
    }
    
}
