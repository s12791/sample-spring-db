
package com.mycompany.test.mvc.spring.dao;

import com.mycompany.test.mvc.spring.model.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonDAO extends CrudRepository<Person, Integer>{
    
}
