
package com.mycompany.test.mvc.spring.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Karolina
 */
@Entity
public class Person {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String lastName;
    @NotEmpty
    @Email
    private String email;
    private int discount;
    
public Integer getId(){
    return id;
}    
public void setId(Integer id){
    this.id=id;
}
public String getName(){
    return name;
}    
public void setName(String name){
    this.name=name;
}
public String getLastName(){
    return lastName;
}    
public void setLastName(String lastName){
    this.lastName=lastName;
}
public String getEmail(){
    return email;
}    
public void setEmail(String email){
    this.email=email;
}
public int getDiscount(){
    return discount;
}    
public void setDiscount(int discount){
    this.discount=discount;
}
    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", name=" + name + ", lastName=" + lastName + ", email=" + email + ", discount=" + discount + '}';
    }

}